
# RoundglassClient

  

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Features Included 
- User Login
- Notification list
- Changing the language from dropdown will also change the language for notification 

## Instruction to strat development server

  -  Clone the repo and install all the dependencies 
  -  Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.