import { Component, OnInit } from '@angular/core';
import { NotificationService, AuthenticationService } from '../services';
import { first } from 'rxjs/operators';
import { User } from '../models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  notifications = <any>[];
  languages = [{
    displayText: "English",
    value: "en"
  }, {
    displayText: "Russian",
    value: "ru"
  }];
  selectedLanguage = ""; 
  currentUser: User;
  constructor(
    private notificationService: NotificationService,
    private authenticationService: AuthenticationService,
  ) { 
    this.currentUser = this.authenticationService.currentUserValue;
    this.selectedLanguage = this.currentUser.contentLanguage;
  }

  ngOnInit() {
    
    this.authenticationService.currentUser.subscribe((user) => {
      // console.log('Current Uers value changed', user)
      this.currentUser = user;
      this.getData();
    });
  }

  getData() {
    this.notificationService.getUserNotifications({language: this.selectedLanguage})
      .pipe(first())
      .subscribe((data: any) => {
        this.notifications = data.data.notifications
      });
  }
  onLanguageSelected(value) {
    this.selectedLanguage = value;
    this.getData();
  }
}
