import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models';
import { environment } from '../../environments/environment';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User { 
        return this.currentUserSubject.value;
    }

    login(email, password) {
        return this.http.post<any>(`${environment.apiUrl}/api/login`, { email, password })
            .pipe(map(user => {
                console.log('user', user)
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                let userInfo = {
                    id: user.data.user._id,
                    email: user.data.user.email,
                    name: user.data.user.name,
                    token: user.data.token,
                    contentLanguage: user.data.user.contentLanguage,
                }
                localStorage.setItem('currentUser', JSON.stringify(userInfo));
                this.currentUserSubject.next(userInfo);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}