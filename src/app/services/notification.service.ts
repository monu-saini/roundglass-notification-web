import { Injectable } from '@angular/core';
import { HttpClient , HttpParams} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class NotificationService {
    constructor(private http: HttpClient) { }

    getUserNotifications(query: any) {
        const params = new HttpParams({
            fromObject: query
        });
        return this.http.get(`${environment.apiUrl}/api/getNotifications`, {params });
    }
}