import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './services';
import { User } from './models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'roundglass-client';
  currentUser: User;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
  ) { 
    this.currentUser = this.authenticationService.currentUserValue;
  }

  ngOnInit() {
    this.authenticationService.currentUser.subscribe((user) => {
      this.currentUser = user;
    });
  }

  logout() {
    this.router.navigate(['/login']);
    this.authenticationService.logout();
  }
}
